### How to learn to Program

 1. Pick a beginner-friendly language
 2. Find getting started tutorial
 3. Set up environment
 4. Code a little every day
 5. Have great idea
 6. Make next killer app
 7. Profit

### Recursively Wrong Way to Learn Programming

 1. Pick five languages
 2. Code as much as you can for a six month cycle
 3. Repeat for one year
 	1. Add five more languages
	2. Cut cycle in half
	3. Laugh maniacally 
	4. Write cryptic warnings in markdown
 4. Profit
